use graphql_template_domain::{error::DomainError, reklamas::errors::*, topics::errors::*};
use juniper::{FieldError, IntoFieldError, ScalarValue};
use std::fmt;

#[derive(Debug, Clone)]
pub enum AppErrorType {
    DomainError,
    NotFoundError,
    InvalidField,
}

pub enum InvalidFieldKind {
    NotaNumber,
}

#[derive(Debug, Clone)]
pub struct AppError {
    pub message: Option<String>,
    pub cause: Option<String>,
    pub error_type: AppErrorType,
}

impl AppError {
    pub fn new_field_err(field: &str, kind: InvalidFieldKind) -> Self {
        match kind {
            InvalidFieldKind::NotaNumber => Self {
                cause: None,
                message: Some(format!("The field: {} must be a number", field)),
                error_type: AppErrorType::InvalidField,
            },
        }
    }

    pub fn message(&self) -> String {
        match &*self {
            AppError {
                message: Some(message),
                ..
            } => message.clone(),
            AppError {
                error_type: AppErrorType::NotFoundError,
                ..
            } => "The requested item was not found".to_owned(),
            AppError {
                error_type: AppErrorType::InvalidField,
                ..
            } => "Invalid field value provided".to_owned(),
            error => {
                println!("{:?}", error);
                "An unexpected error has occurred".to_owned()
            }
        }
    }
}

impl fmt::Display for AppError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(msg) = &self.message {
            write!(f, "{}", msg)
        } else {
            write!(f, "Unexpected error")
        }
    }
}

impl<S: ScalarValue> IntoFieldError<S> for AppError {
    fn into_field_error(self) -> FieldError<S> {
        match self.error_type {
            AppErrorType::DomainError => FieldError::new(
                "Domain error",
                juniper::graphql_value!({
                    "type": "DOMAIN_ERROR"
                }),
            ),
            AppErrorType::InvalidField => FieldError::new(
                "Invalid Field",
                juniper::graphql_value!({
                    "type": "INVALID_FIELD"
                }),
            ),
            AppErrorType::NotFoundError => FieldError::new(
                "Not found",
                juniper::graphql_value!({
                    "type": "NOT_FOUND"
                }),
            ),
        }
    }
}

impl From<DomainError> for AppError {
    fn from(error: DomainError) -> Self {
        AppError {
            message: None,
            cause: error.cause,
            error_type: AppErrorType::DomainError,
        }
    }
}

impl From<GetReklamaError> for AppError {
    fn from(error: GetReklamaError) -> Self {
        match error {
            GetReklamaError::ReklamaNotFound { cause } => Self {
                message: Some("Reklama not found".to_owned()),
                cause: cause,
                error_type: AppErrorType::DomainError,
            },
            GetReklamaError::DomainError(DomainError { cause }) => Self {
                message: None,
                cause: cause,
                error_type: AppErrorType::DomainError,
            },
        }
    }
}

impl From<CreateReklamaError> for AppError {
    fn from(error: CreateReklamaError) -> Self {
        match error {
            CreateReklamaError::StringSize { cause, .. } => Self {
                message: Some("Some string field(s) are too long".to_owned()),
                cause: cause,
                error_type: AppErrorType::DomainError,
            },
            CreateReklamaError::TopicNotFound { cause } => Self {
                message: Some("Referenced topic does not exist".to_owned()),
                cause,
                error_type: AppErrorType::DomainError,
            },
            CreateReklamaError::DomainError(DomainError { cause }) => Self {
                message: None,
                cause,
                error_type: AppErrorType::DomainError,
            },
        }
    }
}

impl From<GetTopicError> for AppError {
    fn from(error: GetTopicError) -> Self {
        match error {
            GetTopicError::TopicNotFound { cause } => Self {
                message: Some("Topic not found".to_owned()),
                cause: cause,
                error_type: AppErrorType::DomainError,
            },
            GetTopicError::DomainError(DomainError { cause }) => Self {
                message: None,
                cause: cause,
                error_type: AppErrorType::DomainError,
            },
        }
    }
}
