use crate::graphql;
use crate::handler;
use juniper::http::graphiql::graphiql_source;
use std::future::Future;
use std::net::SocketAddr;
use std::sync::Arc;
use warp::Filter;

pub struct Config {
    pub graphql_context: graphql::Context,
    pub address: SocketAddr,
}

pub fn start_server(config: Config) -> impl Future<Output = ()> + 'static {
    let graphql_schema = Arc::new(graphql::schema::new());
    let graphql_schema = warp::any().map(move || Arc::clone(&graphql_schema));

    let ctx = Arc::new(config.graphql_context);
    let ctx = warp::any().map(move || Arc::clone(&ctx));

    let graphql_route = warp::post()
        .and(warp::path!("graphql"))
        .and(graphql_schema.clone())
        .and(ctx.clone())
        .and(warp::body::json())
        .and_then(handler::graphql);

    let graphiql_route = warp::get()
        .and(warp::path!("graphiql"))
        .map(|| warp::reply::html(graphiql_source("graphql", None)));

    let routes = graphql_route.or(graphiql_route);

    warp::serve(routes).run(config.address)
}
