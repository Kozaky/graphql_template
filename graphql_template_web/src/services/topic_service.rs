use crate::errors::AppError;
use crate::graphql::Topic;
use graphql_template_db::TopicRepository;
use graphql_template_domain::TopicRepository as TopicRepositoryTrait;

pub struct TopicService {
    topic_repository: TopicRepository,
}

impl TopicService {
    pub fn new(topic_repository: TopicRepository) -> TopicService {
        TopicService { topic_repository }
    }

    pub async fn get_topics(&self) -> Result<Vec<Topic>, AppError> {
        let topics = self
            .topic_repository
            .get_topics()
            .await?
            .iter()
            .map(|topic| Topic::from(topic))
            .collect();

        Ok(topics)
    }

    pub async fn get_topic(&self, id: String) -> Result<Topic, AppError> {
        let id = id.parse::<i64>().expect("Id is not a number");

        let topic = self.topic_repository.get_topic(id).await?;
        let topic = Topic::from(&topic);
        Ok(topic)
    }
}
