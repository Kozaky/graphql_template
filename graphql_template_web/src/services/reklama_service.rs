use crate::errors::AppError;
use crate::graphql::Reklama;
use graphql_template_db::ReklamaRepository;
use graphql_template_domain::reklamas::ReklamaCreate;
use graphql_template_domain::ReklamaRepository as ReklamaRepositoryTrait;

pub struct ReklamaService {
    reklama_repository: ReklamaRepository,
}

impl ReklamaService {
    pub fn new(reklama_repository: ReklamaRepository) -> ReklamaService {
        ReklamaService { reklama_repository }
    }

    pub async fn get_reklamas(&self) -> Result<Vec<Reklama>, AppError> {
        let reklamas = self
            .reklama_repository
            .get_reklamas()
            .await?
            .iter()
            .map(|reklama| Reklama::from(reklama))
            .collect();

        Ok(reklamas)
    }

    pub async fn get_reklama(&self, id: i64) -> Result<Reklama, AppError> {
        let reklama = self.reklama_repository.get_reklama(id).await?;
        let reklama = Reklama::from(&reklama);
        Ok(reklama)
    }

    pub async fn create_reklama(&self, new_reklama: ReklamaCreate) -> Result<Reklama, AppError> {
        let reklama = self.reklama_repository.create_reklama(new_reklama).await?;
        let reklama = Reklama::from(&reklama);
        Ok(reklama)
    }

    pub async fn load_reklamas(&self, id: i64) -> Result<Vec<Reklama>, AppError> {
        let reklamas = self
            .reklama_repository
            .load_reklamas(id)
            .await?
            .iter()
            .map(|reklama| Reklama::from(reklama))
            .collect();

        Ok(reklamas)
    }
}
