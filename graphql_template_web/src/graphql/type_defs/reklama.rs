use chrono::prelude::*;
use graphql_template_domain::reklamas::Reklama as DomainReklama;

#[derive(Clone)]
pub struct Reklama {
    pub id: i64,
    pub title: String,
    pub content: String,
    pub topic_id: i64,
    pub inserted_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[juniper::graphql_object]
impl Reklama {
    fn id(&self) -> String {
        self.id.to_string()
    }

    fn title(&self) -> &str {
        self.title.as_str()
    }

    fn content(&self) -> &str {
        self.content.as_str()
    }

    fn inserted_at(&self) -> String {
        self.inserted_at.format("%Y-%m-%d %H:%M:%S").to_string()
    }

    fn updated_at(&self) -> String {
        self.updated_at.format("%Y-%m-%d %H:%M:%S").to_string()
    }
}

impl From<&DomainReklama> for Reklama {
    fn from(domain_reklama: &DomainReklama) -> Self {
        Self {
            id: domain_reklama.id,
            title: domain_reklama.title.to_owned(),
            content: domain_reklama.content.to_owned(),
            topic_id: domain_reklama.topic_id,
            inserted_at: domain_reklama.inserted_at,
            updated_at: domain_reklama.updated_at,
        }
    }
}
