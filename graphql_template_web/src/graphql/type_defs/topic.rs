use super::Reklama;
use crate::errors::AppError;
use crate::graphql::Context;
use chrono::prelude::*;
extern crate base64;
use graphql_template_domain::topics::Topic as DomainTopic;

#[derive(Clone)]
pub struct Topic {
    pub id: i64,
    pub title: String,
    pub description: String,
    pub image_name: String,
    pub image: Vec<u8>,
    pub inserted_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

#[juniper::graphql_object(
    context = Context
)]
impl Topic {
    fn id(&self) -> String {
        self.id.to_string()
    }

    fn title(&self) -> &str {
        self.title.as_str()
    }

    fn description(&self) -> &str {
        self.description.as_str()
    }

    fn image_name(&self) -> &str {
        self.image_name.as_str()
    }

    fn image(&self) -> String {
        base64::encode(&self.image)
    }

    fn inserted_at(&self) -> String {
        self.inserted_at.format("%Y-%m-%d %H:%M:%S").to_string()
    }

    fn updated_at(&self) -> String {
        self.updated_at.format("%Y-%m-%d %H:%M:%S").to_string()
    }

    async fn reklamas(&self, ctx: &Context) -> Result<Vec<Reklama>, AppError> {
        ctx.reklama_service.load_reklamas(self.id).await
    }
}

impl From<&DomainTopic> for Topic {
    fn from(domain_topic: &DomainTopic) -> Self {
        Self {
            id: domain_topic.id,
            title: domain_topic.title.to_owned(),
            description: domain_topic.description.to_owned(),
            image: domain_topic.image.to_owned(),
            image_name: domain_topic.image_name.to_owned(),
            inserted_at: domain_topic.inserted_at,
            updated_at: domain_topic.updated_at,
        }
    }
}
