use crate::errors::{AppError, InvalidFieldKind};
use crate::graphql::{Context, Reklama, Topic};

/// Struct for GraphQL queries
pub struct QueryRoot;

#[juniper::graphql_object(Context = Context)]
impl QueryRoot {
    // Reklamas
    async fn reklamas(ctx: &Context) -> Result<Vec<Reklama>, AppError> {
        ctx.reklama_service.get_reklamas().await
    }

    async fn reklama(ctx: &Context, id: String) -> Result<Reklama, AppError> {
        let id = id
            .parse::<i64>()
            .map_err(|_| AppError::new_field_err("topic_id", InvalidFieldKind::NotaNumber))?;

        ctx.reklama_service.get_reklama(id).await
    }

    // Topics
    async fn topics(ctx: &Context) -> Result<Vec<Topic>, AppError> {
        ctx.topic_service.get_topics().await
    }

    async fn topic(ctx: &Context, id: String) -> Result<Topic, AppError> {
        ctx.topic_service.get_topic(id).await
    }
}
