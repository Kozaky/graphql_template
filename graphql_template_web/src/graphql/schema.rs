use crate::graphql::{MutationRoot, QueryRoot};
use crate::services::{ReklamaService, TopicService};
use juniper::{RootNode, EmptySubscription};

pub type Schema = RootNode<'static, QueryRoot, MutationRoot, EmptySubscription<Context>>;

pub fn new() -> Schema {
    Schema::new(QueryRoot, MutationRoot, EmptySubscription::<Context>::new())
}

/// Context shared in all GraphQL queries
pub struct Context {
    pub reklama_service: ReklamaService,
    pub topic_service: TopicService,
}

impl juniper::Context for Context {}
