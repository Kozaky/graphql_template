use crate::errors::{AppError, InvalidFieldKind};
use crate::graphql::{Context, Reklama};
use graphql_template_domain::reklamas::ReklamaCreate;

/// Struct for GraphQL mutations
pub struct MutationRoot;

#[juniper::graphql_object(Context = Context)]
impl MutationRoot {
    // Reklamas
    async fn create_reklama(
        ctx: &Context,
        title: String,
        content: String,
        topic_id: String,
    ) -> Result<Reklama, AppError> {
        let topic_id = topic_id
            .parse::<i64>()
            .map_err(|_| AppError::new_field_err("topic_id", InvalidFieldKind::NotaNumber))?;

        let new_reklama = ReklamaCreate {
            title,
            content,
            topic_id,
        };

        ctx.reklama_service.create_reklama(new_reklama).await
    }

    // Topics
}
