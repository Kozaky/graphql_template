use crate::error::DomainError;
use crate::topics::{errors::*, Topic};
use async_trait::async_trait;

#[async_trait]
pub trait TopicRepository {
    async fn get_topics(&self) -> Result<Vec<Topic>, DomainError>;
    async fn get_topic(&self, id: i64) -> Result<Topic, GetTopicError>;
}
