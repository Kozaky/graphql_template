use chrono::prelude::NaiveDateTime;

pub struct Topic {
    pub id: i64,
    pub title: String,
    pub description: String,
    pub image_name: String,
    pub image: Vec<u8>,
    pub inserted_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}
