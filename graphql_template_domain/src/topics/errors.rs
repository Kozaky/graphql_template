use crate::error::DomainError;

#[derive(Debug)]
pub enum GetTopicError {
    TopicNotFound { cause: Option<String> },
    DomainError(DomainError),
}
