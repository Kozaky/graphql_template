use crate::error::DomainError;
use crate::reklamas::{errors::*, Reklama, ReklamaCreate};
use async_trait::async_trait;

#[async_trait]
pub trait ReklamaRepository {
    async fn get_reklamas(&self) -> Result<Vec<Reklama>, DomainError>;
    async fn get_reklama(&self, id: i64) -> Result<Reklama, GetReklamaError>;
    async fn create_reklama(
        &self,
        new_reklama: ReklamaCreate,
    ) -> Result<Reklama, CreateReklamaError>;
    async fn load_reklamas(&self, id: i64) -> Result<Vec<Reklama>, DomainError>;
}
