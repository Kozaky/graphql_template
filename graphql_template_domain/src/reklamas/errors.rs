use crate::error::DomainError;

#[derive(Debug)]
pub enum GetReklamaError {
    ReklamaNotFound { cause: Option<String> },
    DomainError(DomainError),
}

#[derive(Debug)]
pub enum CreateReklamaError {
    StringSize { cause: Option<String> },
    TopicNotFound { cause: Option<String> },
    DomainError(DomainError),
}
