use chrono::prelude::NaiveDateTime;

#[derive(Clone)]
pub struct Reklama {
    pub id: i64,
    pub title: String,
    pub content: String,
    pub topic_id: i64,
    pub inserted_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
}

pub struct ReklamaCreate {
    pub title: String,
    pub content: String,
    pub topic_id: i64,
}
