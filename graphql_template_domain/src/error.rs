#[derive(Clone, Debug)]
pub struct DomainError {
    pub cause: Option<String>,
}
