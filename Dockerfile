FROM rust:latest as builder

RUN USER=root cargo new graphql_template
RUN pwd
WORKDIR ./graphql_template
RUN pwd
RUN USER=root cargo new --bin graphql_template_application
RUN USER=root cargo new --lib graphql_template_db
RUN USER=root cargo new --lib graphql_template_domain
RUN USER=root cargo new --lib graphql_template_web
COPY ./Cargo.toml ./Cargo.toml
COPY ./graphql_template_application/Cargo.toml ./graphql_template_application/Cargo.toml
COPY ./graphql_template_db/Cargo.toml ./graphql_template_db/Cargo.toml
COPY ./graphql_template_domain/Cargo.toml ./graphql_template_domain/Cargo.toml
COPY ./graphql_template_web/Cargo.toml ./graphql_template_web/Cargo.toml
RUN cargo build --release
RUN rm graphql_template_application/src/*.rs
RUN rm graphql_template_db/src/*.rs
RUN rm graphql_template_domain/src/*.rs
RUN rm graphql_template_web/src/*.rs

ADD . ./

RUN cargo clean -p graphql_template_application --release
RUN cargo clean -p graphql_template_db --release
RUN cargo clean -p graphql_template_domain --release
RUN cargo clean -p graphql_template_web --release
RUN cargo build --release



FROM debian:buster-slim
ARG APP=/usr/src/app

RUN apt-get update \
    && apt-get install -y ca-certificates tzdata \
    && rm -rf /var/lib/apt/lists/*

EXPOSE 4040

ENV TZ=Etc/UTC \
    APP_USER=appuser

RUN groupadd $APP_USER \
    && useradd -g $APP_USER $APP_USER \
    && mkdir -p ${APP}

COPY --from=builder /graphql_template/target/release/graphql_template_application ${APP}/graphql_template

RUN chown -R $APP_USER:$APP_USER ${APP}

USER $APP_USER
WORKDIR ${APP}


CMD ["./graphql_template"]