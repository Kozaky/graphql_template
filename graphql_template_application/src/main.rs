use graphql_template_db::{Connection, ReklamaRepository, TopicRepository};
use std::sync::Arc;
use graphql_template_web::{
    graphql,
    server::{start_server, Config},
    services::{ReklamaService, TopicService},
};

#[tokio::main]
async fn main() {
    let server_address = dotenv::var("SERVER_ADDRESS")
        .expect("SERVER_ADDRESS not present")
        .parse()
        .unwrap();

    let config = Config {
        graphql_context: create_context(),
        address: server_address,
    };

    println!("Started server at {}", config.address);
    start_server(config).await;
}

fn create_context() -> graphql::Context {
    let url = dotenv::var("DATABASE_URL").expect("DATABASE_URL not present");
    let db_con = Connection::new(&url);
    let db_con = Arc::new(db_con);

    let reklama_repository = ReklamaRepository::new(Arc::clone(&db_con));
    let topic_repository = TopicRepository::new(db_con);

    let reklama_service = ReklamaService::new(reklama_repository);
    let topic_service = TopicService::new(topic_repository);

    graphql::Context {
        reklama_service,
        topic_service,
    }
}
