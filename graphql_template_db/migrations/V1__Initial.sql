-- TOPICS 

CREATE TABLE topics (
	id bigserial NOT NULL,
	title varchar(50) NOT NULL,
	description varchar(100) NOT NULL,
	image_name varchar(50) NOT NULL,
	image bytea NOT NULL,
	inserted_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	CONSTRAINT topics_pkey PRIMARY KEY (id)
);
CREATE UNIQUE INDEX topics_title_index ON public.topics USING btree (title);

-- Insert default topics

INSERT INTO topics (id, title, description, image_name, image, inserted_at, updated_at) VALUES(1, 'General', 'All complaints are initailize with this topic till they are reorganized', 'spanish_square.jpg', '000', '2020-05-01 14:02:50.000', '2020-05-01 18:27:50.000');

-- REKLAMAS

CREATE TABLE reklamas (
	id bigserial NOT NULL,
	title varchar(100) NOT NULL,
	"content" varchar(3000) NOT NULL,
	user_id int8 NOT NULL,
	topic_id int8 NOT NULL,
	inserted_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	CONSTRAINT reklamas_pkey PRIMARY KEY (id)
);

-- reklamas foreign keys

ALTER TABLE reklamas ADD CONSTRAINT reklamas_topic_id_fkey FOREIGN KEY (topic_id) REFERENCES topics(id) ON DELETE CASCADE;
-- ALTER TABLE reklamas ADD CONSTRAINT reklamas_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);

INSERT INTO reklamas (title,"content",user_id,topic_id,inserted_at,updated_at) VALUES 
('Title1','Description of your complaint1',2,1,'2020-05-01 18:22:15.000','2020-05-01 18:22:15.000')
,('Title2','Description of your complaint2',2,1,'2020-05-01 18:22:24.000','2020-05-01 18:22:24.000')
,('Title3','Description of your complaint3',2,1,'2020-05-01 18:28:24.000','2020-05-01 18:28:24.000')
,('Title4','Description of your complaint4',2,1,'2020-05-01 18:28:35.000','2020-05-01 18:28:35.000')
,('Title5','Description of your complaint5',2,1,'2020-05-01 18:28:42.000','2020-05-01 18:28:42.000')
,('Title6','Description of your complaint6',2,1,'2020-05-01 18:29:00.000','2020-05-01 18:29:00.000')
,('Title7','Description of your complaint7',2,1,'2020-05-01 18:29:13.000','2020-05-01 18:29:13.000')
,('Title8','Description of your complaint8',2,1,'2020-05-01 18:29:26.000','2020-05-01 18:29:26.000');