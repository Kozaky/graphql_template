use crate::connection::DBPool;
use crate::errors::DBError;
use chrono::prelude::*;
use graphql_template_domain::reklamas::ReklamaCreate;
use tokio_postgres::Row;

pub struct ReklamaRow(pub Row);

impl From<Row> for ReklamaRow {
    fn from(row: Row) -> Self {
        ReklamaRow(row)
    }
}

pub async fn get_reklamas(db_pool: &DBPool) -> Result<Vec<ReklamaRow>, DBError> {
    let con = db_pool.get().await?;
    let rows = con
        .query(
            "SELECT id, title, content, topic_id, inserted_at, updated_at FROM reklamas",
            &[],
        )
        .await?
        .into_iter()
        .map(|row| ReklamaRow::from(row))
        .collect();

    Ok(rows)
}

pub async fn get_reklama(db_pool: &DBPool, id: i64) -> Result<ReklamaRow, DBError> {
    let con = db_pool.get().await?;
    let row = con
        .query_one(
            "SELECT id, title, content, topic_id, inserted_at, updated_at FROM reklamas WHERE id = $1",
            &[&id]
        )
        .await?;

    Ok(ReklamaRow::from(row))
}

pub async fn create_reklama(
    db_pool: &DBPool,
    new_reklama: ReklamaCreate,
) -> Result<ReklamaRow, DBError> {
    let con = db_pool.get().await?;
    let inserted_at = Utc::now().naive_utc();

    let row = con
        .query_one(
            "INSERT INTO reklamas (title, content, user_id, topic_id, inserted_at, updated_at) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id, title, content, topic_id, inserted_at, updated_at",
            &[&new_reklama.title, &new_reklama.content, &1i64, &new_reklama.topic_id, &inserted_at, &inserted_at]
        )
        .await?;

    Ok(ReklamaRow::from(row))
}

pub async fn get_reklamas_by_topic_ids(
    db_pool: &DBPool,
    ids: Vec<i64>,
) -> Result<Vec<ReklamaRow>, DBError> {
    let con = db_pool.get().await?;

    let rows = con
        .query(
            "SELECT id, title, content, topic_id, inserted_at, updated_at FROM reklamas WHERE topic_id = ANY($1)",
            &[&ids]
        )
        .await?
        .into_iter()
        .map(|row| ReklamaRow::from(row))
        .collect();

    Ok(rows)
}
