use crate::connection::DBPool;
use crate::errors::DBError;
use tokio_postgres::Row;

pub struct TopicRow(pub Row);

impl From<Row> for TopicRow {
    fn from(row: Row) -> Self {
        TopicRow(row)
    }
}

pub async fn get_topics(db_pool: &DBPool) -> Result<Vec<TopicRow>, DBError> {
    let con = db_pool.get().await?;
    let rows = con
        .query(
            "SELECT id, title, description, image_name, image, inserted_at, updated_at FROM topics",
            &[],
        )
        .await?
        .into_iter()
        .map(|row| TopicRow::from(row))
        .collect();

    Ok(rows)
}

pub async fn get_topic(db_pool: &DBPool, id: i64) -> Result<TopicRow, DBError> {
    let con = db_pool.get().await?;
    let row = con
        .query_one(
            "SELECT id, title, description, image_name, image, inserted_at, updated_at FROM topics WHERE id = $1",
            &[&id],
        )
        .await?;

    Ok(TopicRow::from(row))
}
