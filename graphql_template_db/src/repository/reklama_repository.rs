use crate::connection::Connection;
use crate::queries::{reklama_queries, reklama_queries::ReklamaRow};
use async_trait::async_trait;
use dataloader::{non_cached::Loader, BatchFn};
use graphql_template_domain::{
    error::DomainError,
    reklamas::{errors::*, Reklama, ReklamaCreate},
};
use std::{collections::HashMap, sync::Arc};

pub struct ReklamaRepository {
    pub con: Arc<Connection>,
    pub reklama_loader: ReklamaLoader,
}

impl ReklamaRepository {
    pub fn new(con: Arc<Connection>) -> Self {
        Self {
            reklama_loader: Loader::new(ReklamaBatcher {
                con: Arc::clone(&con),
            })
            .with_yield_count(100),
            con,
        }
    }
}

#[async_trait]
impl graphql_template_domain::ReklamaRepository for ReklamaRepository {
    async fn get_reklamas(&self) -> Result<Vec<Reklama>, DomainError> {
        let reklamas = reklama_queries::get_reklamas(&self.con.db_pool)
            .await?
            .into_iter()
            .map(|row| Reklama::from(row))
            .collect();

        Ok(reklamas)
    }

    async fn get_reklama(&self, id: i64) -> Result<Reklama, GetReklamaError> {
        let row = reklama_queries::get_reklama(&self.con.db_pool, id).await?;
        Ok(Reklama::from(row))
    }

    async fn create_reklama(
        &self,
        new_reklama: ReklamaCreate,
    ) -> Result<Reklama, CreateReklamaError> {
        let row = reklama_queries::create_reklama(&self.con.db_pool, new_reklama).await?;

        Ok(Reklama::from(row))
    }

    async fn load_reklamas(&self, id: i64) -> Result<Vec<Reklama>, DomainError> {
        self.reklama_loader.load(id).await
    }
}

pub type ReklamaLoader = Loader<i64, Result<Vec<Reklama>, DomainError>, ReklamaBatcher>;

pub struct ReklamaBatcher {
    pub con: Arc<Connection>,
}

impl ReklamaBatcher {
    pub async fn get_reklamas_by_topic_ids(
        &self,
        hashmap: &mut HashMap<i64, Vec<Reklama>>,
        ids: Vec<i64>,
    ) -> Result<(), DomainError> {
        reklama_queries::get_reklamas_by_topic_ids(&self.con.db_pool, ids)
            .await?
            .into_iter()
            .map(|row| Reklama::from(row))
            .collect::<Vec<Reklama>>()
            .iter()
            .fold(
                hashmap,
                |map: &mut HashMap<i64, Vec<Reklama>>, reklama: &Reklama| {
                    let vec = map
                        .entry(reklama.topic_id)
                        .or_insert_with(|| Vec::<Reklama>::new());
                    vec.push(reklama.clone());
                    map
                },
            );

        Ok(())
    }
}

#[async_trait]
impl BatchFn<i64, Result<Vec<Reklama>, DomainError>> for ReklamaBatcher {
    async fn load(&mut self, keys: &[i64]) -> HashMap<i64, Result<Vec<Reklama>, DomainError>> {
        let mut reklamas_map = HashMap::new();

        let result: Result<(), DomainError> = self
            .get_reklamas_by_topic_ids(&mut reklamas_map, keys.to_owned())
            .await;

        keys.iter()
            .map(move |id| {
                let entry = reklamas_map.entry(*id).or_insert_with(|| vec![]).clone();

                (id.clone(), result.clone().map(|_| entry))
            })
            .collect::<HashMap<_, _>>()
    }
}

impl From<ReklamaRow> for Reklama {
    fn from(reklama_row: ReklamaRow) -> Self {
        Reklama {
            id: reklama_row.0.get(0),
            title: reklama_row.0.get(1),
            content: reklama_row.0.get(2),
            topic_id: reklama_row.0.get(3),
            inserted_at: reklama_row.0.get(4),
            updated_at: reklama_row.0.get(5),
        }
    }
}
