mod reklama_repository;
mod topic_repository;

pub use reklama_repository::ReklamaRepository;
pub use topic_repository::TopicRepository;
