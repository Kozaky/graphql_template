use crate::connection::Connection;
use crate::queries::{topic_queries, topic_queries::TopicRow};
use async_trait::async_trait;
use graphql_template_domain::{
    error::DomainError,
    topics::{errors::*, Topic},
};
use std::sync::Arc;

pub struct TopicRepository {
    pub con: Arc<Connection>,
}

impl TopicRepository {
    pub fn new(con: Arc<Connection>) -> Self {
        Self { con }
    }
}

#[async_trait]
impl graphql_template_domain::TopicRepository for TopicRepository {
    async fn get_topics(&self) -> Result<Vec<Topic>, DomainError> {
        let topics = topic_queries::get_topics(&self.con.db_pool)
            .await?
            .into_iter()
            .map(|row| Topic::from(row))
            .collect();

        Ok(topics)
    }

    async fn get_topic(&self, id: i64) -> Result<Topic, GetTopicError> {
        let row = topic_queries::get_topic(&self.con.db_pool, id).await?;

        Ok(Topic::from(row))
    }
}

impl From<TopicRow> for Topic {
    fn from(topic_row: TopicRow) -> Self {
        Topic {
            id: topic_row.0.get(0),
            title: topic_row.0.get(1),
            description: topic_row.0.get(2),
            image_name: topic_row.0.get(3),
            image: topic_row.0.get(4),
            inserted_at: topic_row.0.get(5),
            updated_at: topic_row.0.get(6),
        }
    }
}
